//users.tsx
import axios from "axios";
import { List, useNotify, Datagrid, TextField, EmailField, SimpleForm, Create, TextInput, SaveButton, DeleteButton, PasswordInput } from "react-admin";
import { useState } from 'react';

export const UserList1 = () => (
    
    <List disableAuthentication>
      <Datagrid >
        <TextField source="id"  />
        <TextField source="name" />
        <EmailField source="email" />
      </Datagrid>
    </List>
);

export const UserCreate = () => {

  /*const [id, setId] = useState('');*/
  const [name, setName] = useState("");
  const [address, setEmail] = useState("");
  const [mobile, setPassword] = useState("");
  /*const [apikey, setApi] = useState("");*/
  const notify=useNotify();
  async function save(event: any) {
    event.preventDefault();
    try {
      await axios.post("http://providers.aldia.site:8080/api-providers/providers/register",
        {
          name: name,
          email: address,
          password: mobile

        });
      notify(`Proveedor registrado con exito!!`, { type: 'success' });
     
      //window.location.reload();
      setTimeout(function(){
        window.location.reload();
     }, 1000);  

      /*setId("");*/
      setName("");
      setEmail("");
      setPassword("");
      /*setApi("");*/
    }
    catch (err) {    
      notify(`ERROR: Ocurrio un evento inesperado en la creacion del proveedor`, { type: 'error' });
    }
  }
  return (
    <Create disableAuthentication>
      <SimpleForm toolbar={<DeleteButton />}>
        <TextInput source="Nombre" value={name} onChange={(event) => { setName(event.target.value); }} />
        <TextInput type="email" source="Email" value={address} onChange={(event) => { setEmail(event.target.value); }} />
        <PasswordInput source="Password" value={mobile} onChange={(event) => { setPassword(event.target.value); }} />
        <SaveButton onClick={save} />
      </SimpleForm>
    </Create>

  );
}