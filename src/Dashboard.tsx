import { Card, CardContent, CardHeader } from "@mui/material";

export const Dashboard = () => (
  <Card>
    <CardHeader title="Bienvenidos a AlDiApp" />
    <CardContent>El objetivo del proyecto AldiApp es 
      construir una plataforma que opere como SaaS en 
      donde las personas se puedan registrar y poder 
      llevar un registro periódicamente de los pagos 
      y obligaciones financieras con las que cuenta. 
      La plataforma permitirá que los usuarios se registren 
      y registren contratos u vínculos que generen obligaciones 
      periódicas, esto con el fin de llevar un control 
      de los pagos que debe realizar y recibir información 
      de las empresas vinculantes.</CardContent>
  </Card>
);

