//users.tsx

import { List, Datagrid, TextField, EmailField } from "react-admin";

export const UserList = () => (
    
  <List disableAuthentication>
    <Datagrid >
      <TextField source="idUser"  />
      <TextField source="idState" />
      <TextField source="lastname"  />
      <TextField source="name" />
      <EmailField source="email" />
      <TextField source="phone"  />
      <TextField source="enabled" />
    </Datagrid>
  </List>
);
