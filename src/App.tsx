import { Admin, Resource } from 'react-admin';
/*import jsonServerProvider from 'ra-data-json-server';
import {dataProvider} from './dataProvider';
import {dataProvider1} from './dataProvider1';
import {dataProvider2} from './dataProvider2';*/
import { UserCreate, UserList1} from './provider';
import { UserList} from './users';
import { Dashboard } from './Dashboard';
import { useEffect, useState } from 'react';
import { NotifyCreate } from './notify';
import {dataProvider} from './dataProvider';
import {dataProvider1} from './dataProvider1';

export const App = () => {

/*const dataProvider = jsonServerProvider('http://users.aldia.site:8090/api-users');
const dataProvider1 = jsonServerProvider('http://providers.aldia.site:8080/api-providers/providers');
const dataProvider2 = jsonServerProvider('http://providers.aldia.site:8080/api-providers/providers/2');

/*const dataProvider3 = jsonServerProvider('http://users.aldia.site:8090/api-users/users');
const user = localStorage.getItem('user');*/

/*const dataProvider = jsonServerProvider('http://providers.aldia.site:8080/api-providers/providers');*/
const username = localStorage.getItem('username');
const [userId, setUserId] = useState(null);
  
  useEffect(() => {
    // Realiza la solicitud a tu API para obtener la lista de proveedores
    fetch('http://providers.aldia.site:8080/api-providers/providers/all')
      .then(response => response.json())
      .then(data => {
        const user = data.find((user) => user.email === username);
        if (user) {
          setUserId(user.id);
        }
      })
      .catch(error => {
        console.error('Error al obtener el ID del proveedor', error);
      });
  }, [username]);

  return (
      <Admin dashboard={Dashboard} dataProvider={dataProvider1}>
        <Resource name="users" 
                  list={UserList} 
                  options={{ label: "Usuarios" }} 
                  dataProvider={dataProvider} />
        <Resource name="all" 
                  list={UserList1} 
                  create={UserCreate} 
                  options={{ label: "Proveedores" }} 
                  /*dataProvider={dataProvider1}*//>
        <Resource name="notify" 
                  list={NotifyCreate} 
                  options={{ label: "Notificaciones" }} 
                  /*dataProvider={dataProvider2}*/ />
      </Admin>
    ); 
};




/*

<Resource name={userId + '/users'} list={SusbcriptorList} options={{ label: "Usuarios" }} />
<Resource name="all" list={UserList} create={UserCreate} options={{ label: "Proveedores" }} />
<Resource name="sendMassiveNotify" list={NotifyCreate} options={{ label: "Notificaciones" }} />

export const App = () => (
  <Admin dataProvider={dataProvider}>
    <Resource name="api-providers/providers/all" list={UserList} options={{label: 'Usuario'}}/>
    <Resource name="posts"  list={PostList} edit={PostEdit} create={PostCreate} options={{ label: 'Provider' } }  />
    <Resource name="notify" create={NotifyCreate}/>
  </Admin>
);*/